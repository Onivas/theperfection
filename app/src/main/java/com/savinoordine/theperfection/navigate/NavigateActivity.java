package com.savinoordine.theperfection.navigate;


import android.os.Bundle;

import com.savinoordine.theperfection.R;
import com.savinoordine.theperfection.base.BaseActivity;

public class NavigateActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_navigate;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_navigate;
    }
}
