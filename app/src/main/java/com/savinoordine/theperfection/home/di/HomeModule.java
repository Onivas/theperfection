package com.savinoordine.theperfection.home.di;

import com.savinoordine.theperfection.home.HomeActivity;
import com.savinoordine.theperfection.home.HomeContract;
import com.savinoordine.theperfection.home.HomePresenter;

import dagger.Binds;
import dagger.Module;

@Module
interface HomeModule {

    @Binds
    HomeContract.Presenter providePresenter(HomePresenter presenter);


    @Binds
    HomeContract.View provideView(HomeActivity activity);

}
