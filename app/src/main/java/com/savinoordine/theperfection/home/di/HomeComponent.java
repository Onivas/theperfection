package com.savinoordine.theperfection.home.di;

import com.savinoordine.theperfection.home.HomeActivity;
import com.savinoordine.theperfection.di.component.SubcomponentBuilder;
import com.savinoordine.theperfection.di.scope.ActivityScope;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = HomeModule.class)
public interface HomeComponent extends com.savinoordine.theperfection.di.component.Subcomponent<HomeActivity> {

    @Subcomponent.Builder
    interface Builder extends SubcomponentBuilder<HomeActivity, HomeComponent> {
    }
}
