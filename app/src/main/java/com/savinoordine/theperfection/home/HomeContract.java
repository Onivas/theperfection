package com.savinoordine.theperfection.home;


import com.savinoordine.theperfection.model.Stargazer;

import java.util.List;

public interface HomeContract {

    interface Presenter {

        void pickupRandomMessage();

        /**
         * Receive username and repository name
         * @param user
         * @param repository
         */
        void sendUserAndRepo(String user, String repository);
    }

    interface View {

        void showSnackbarMessage(int resId);

        void setStargazers(List<Stargazer> stargazers);
    }
}
