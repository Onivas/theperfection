package com.savinoordine.theperfection.home.service;

import com.savinoordine.theperfection.model.Stargazer;
import com.savinoordine.theperfection.networking.ApiManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class HomeService {

    private ApiManager mApiManager;

    @Inject
    public HomeService(ApiManager apiManager) {
        mApiManager = apiManager;
    }

    public Single<List<Stargazer>> getStargazerList(String user, String repository) {
        return mApiManager.getStargazers(user, repository);
    }
}
