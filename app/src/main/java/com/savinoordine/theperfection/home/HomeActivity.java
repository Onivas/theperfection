package com.savinoordine.theperfection.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.savinoordine.theperfection.MyApplication;
import com.savinoordine.theperfection.R;
import com.savinoordine.theperfection.base.BaseActivity;
import com.savinoordine.theperfection.model.Stargazer;
import com.savinoordine.theperfection.networking.ApiManager;

import java.util.List;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity implements HomeContract.View {

    @Inject
    ApiManager mApiManager;

    @Inject
    HomeContract.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inject();

        final TextInputEditText repositoryValue = findViewById(R.id.repo_edittext);
        final TextInputEditText usernameValue = findViewById(R.id.user_edittext);

        Button searchButton = findViewById(R.id.search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = usernameValue.getText().toString();
                String repository = repositoryValue.getText().toString();
                if (!TextUtils.isEmpty(user) && !TextUtils.isEmpty(repository)) {
                    mPresenter.sendUserAndRepo(user, repository);
                } else {
                    showSnackbarMessage(R.string.empty_field_not_allowed);
                }
            }
        });

    }

    private void inject() {
        MyApplication.from(this)
                .getSubcomponentBuilder(this)
                .instance(this)
                .build()
                .inject(this);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_home;
    }

    @Override
    public void showSnackbarMessage(@StringRes int resId) {
        Snackbar.make(mContainer, resId, BaseTransientBottomBar.LENGTH_LONG).show();
    }

    @Override
    public void setStargazers(List<Stargazer> stargazers) {
        Log.d("TAG: stargazers.size = ", String.valueOf(stargazers.size()));
    }
}
