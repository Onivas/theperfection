package com.savinoordine.theperfection.home;

import com.savinoordine.theperfection.R;
import com.savinoordine.theperfection.home.service.HomeService;
import com.savinoordine.theperfection.model.Stargazer;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View mView;
    private HomeService mService;

    @Inject
    public HomePresenter(HomeContract.View view, HomeService service) {
        mView = view;
        mService = service;
    }

    @Override
    public void pickupRandomMessage() {

    }

    @Override
    public void sendUserAndRepo(String user, String repository) {
        mService.getStargazerList(user, repository)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Stargazer>>() {
                    @Override
                    public void accept(List<Stargazer> stargazers) throws Exception {
                        if (stargazers.size() == 0) {
                            mView.showSnackbarMessage(R.string.empty_result);
                        } else {
                            mView.setStargazers(stargazers);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mView.showSnackbarMessage(R.string.generic_error);
                    }
                });
    }
}
