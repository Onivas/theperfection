package com.savinoordine.theperfection.database;


import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    DatabaseHelper provideDatabaseHelper(Context context){
        return new DatabaseHelper(context);
    }
}
