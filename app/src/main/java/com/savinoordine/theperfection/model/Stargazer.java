package com.savinoordine.theperfection.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stargazer {

    @SerializedName("login")
    @Expose
    private String login;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;

    @SerializedName("gravatar_id")
    @Expose
    private String gravatarId;

    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("html_url")
    @Expose
    private String htmlUrl;

    @SerializedName("followers_url")
    @Expose
    private String followersUrl;

    @SerializedName("following_url")
    @Expose
    private String followingUrl;

    @SerializedName("gists_url")
    @Expose
    private String gistsUrl;

    @SerializedName("starred_url")
    @Expose
    private String organizationsUrl;

    @SerializedName("repos_url")
    @Expose
    private String reposUrl;

    private Stargazer(String login, Integer id, String avatarUrl, String url) {
        this.login = login;
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.url = url;
    }

    public String getLogin() {
        return login;
    }

    public Integer getId() {
        return id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getGravatarId() {
        return gravatarId;
    }

    public String getUrl() {
        return url;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getFollowersUrl() {
        return followersUrl;
    }

    public String getFollowingUrl() {
        return followingUrl;
    }

    public String getGistsUrl() {
        return gistsUrl;
    }

    public String getOrganizationsUrl() {
        return organizationsUrl;
    }

    public String getReposUrl() {
        return reposUrl;
    }

    public static class Builder {
        String mLogin;
        int mId;
        String mAvatarUrl;
        String mGravatarId;
        String mUrl;
        String mHtmlUrl;
        String mFollowersUrl;
        String mFollowingUrl;
        String mGistsUrl;
        String mOrganizationsUrl;
        String mReposUrl;

        public Builder addLogin(String login) {
            mLogin = login;
            return this;
        }

        //....more

        public Stargazer build() {
            return new Stargazer(mLogin, mId, mAvatarUrl, mUrl);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stargazer)) return false;

        Stargazer stargazer = (Stargazer) o;

        if (login != null ? !login.equals(stargazer.login) : stargazer.login != null) return false;
        if (id != null ? !id.equals(stargazer.id) : stargazer.id != null) return false;
        if (avatarUrl != null ? !avatarUrl.equals(stargazer.avatarUrl) : stargazer.avatarUrl != null)
            return false;
        if (gravatarId != null ? !gravatarId.equals(stargazer.gravatarId) : stargazer.gravatarId != null)
            return false;
        if (url != null ? !url.equals(stargazer.url) : stargazer.url != null) return false;
        if (htmlUrl != null ? !htmlUrl.equals(stargazer.htmlUrl) : stargazer.htmlUrl != null)
            return false;
        if (followersUrl != null ? !followersUrl.equals(stargazer.followersUrl) : stargazer.followersUrl != null)
            return false;
        if (followingUrl != null ? !followingUrl.equals(stargazer.followingUrl) : stargazer.followingUrl != null)
            return false;
        if (gistsUrl != null ? !gistsUrl.equals(stargazer.gistsUrl) : stargazer.gistsUrl != null)
            return false;
        if (organizationsUrl != null ? !organizationsUrl.equals(stargazer.organizationsUrl) : stargazer.organizationsUrl != null)
            return false;
        return reposUrl != null ? reposUrl.equals(stargazer.reposUrl) : stargazer.reposUrl == null;

    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (avatarUrl != null ? avatarUrl.hashCode() : 0);
        result = 31 * result + (gravatarId != null ? gravatarId.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (htmlUrl != null ? htmlUrl.hashCode() : 0);
        result = 31 * result + (followersUrl != null ? followersUrl.hashCode() : 0);
        result = 31 * result + (followingUrl != null ? followingUrl.hashCode() : 0);
        result = 31 * result + (gistsUrl != null ? gistsUrl.hashCode() : 0);
        result = 31 * result + (organizationsUrl != null ? organizationsUrl.hashCode() : 0);
        result = 31 * result + (reposUrl != null ? reposUrl.hashCode() : 0);
        return result;
    }
}
