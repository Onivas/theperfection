package com.savinoordine.theperfection.di.component;

import com.savinoordine.theperfection.home.HomeActivity;
import com.savinoordine.theperfection.home.di.HomeComponent;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = HomeComponent.class)
interface SubcomponentsModule {
    
    @Binds
    @IntoMap
    @ClassKey(HomeActivity.class)
    SubcomponentBuilder homeComponentBuilder(HomeComponent.Builder builder);
}
