package com.savinoordine.theperfection.di.component;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.savinoordine.theperfection.MyApplication;
import com.savinoordine.theperfection.database.DatabaseModule;
import com.savinoordine.theperfection.di.module.AndroidModule;
import com.savinoordine.theperfection.home.service.HomeService;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(
        modules = {AndroidModule.class, DatabaseModule.class, SubcomponentsModule.class})
public interface AndroidComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        Builder databaseModule(DatabaseModule databaseModule);

        AndroidComponent build();
    }

    void inject(MyApplication subito);

    Application application();

    Context applicationContext();

    SharedPreferences sharePreferances();

    HomeService homeService();
}
