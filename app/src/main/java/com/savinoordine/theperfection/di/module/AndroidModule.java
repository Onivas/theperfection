package com.savinoordine.theperfection.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.savinoordine.theperfection.networking.ApiManager;
import com.savinoordine.theperfection.networking.NetworkManager;
import com.savinoordine.theperfection.networking.service.StargazerService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AndroidModule {

    @Provides
    static Context provideApplicationContext(Application application) {
        return application.getApplicationContext();
    }

    @Provides
    static SharedPreferences providesSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Singleton
    @Provides
    public Retrofit retrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }

    @Provides
    public StargazerService stargazerService() {
        return retrofit().create(StargazerService.class);
    }

    @Provides
    public ApiManager apiManager(StargazerService stargazerService) {
        return new NetworkManager(stargazerService);
    }
}
