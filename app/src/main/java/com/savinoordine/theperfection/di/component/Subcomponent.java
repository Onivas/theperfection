package com.savinoordine.theperfection.di.component;

public interface Subcomponent<I> {
    void inject(I instance);
}
