package com.savinoordine.theperfection.di.component;

import dagger.BindsInstance;

public interface SubcomponentBuilder<I, S extends Subcomponent<I>> {
    @BindsInstance
    SubcomponentBuilder<I, S> instance(I instance);

    S build();
}
