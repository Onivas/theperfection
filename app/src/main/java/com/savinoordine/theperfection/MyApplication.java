package com.savinoordine.theperfection;


import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.savinoordine.theperfection.di.component.AndroidComponent;
import com.savinoordine.theperfection.di.component.DaggerAndroidComponent;
import com.savinoordine.theperfection.di.component.Subcomponent;
import com.savinoordine.theperfection.di.component.SubcomponentBuilder;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class MyApplication extends Application {

    private AndroidComponent mAndroidComponent;

    @Inject
    Map<Class<?>, SubcomponentBuilder> mSubcomponentBuilders;

    @Override
    public void onCreate() {
        super.onCreate();

        mAndroidComponent = createComponent();
    }

    private AndroidComponent createComponent() {

        mAndroidComponent = DaggerAndroidComponent.builder()
                .application(this)
                .build();

        mAndroidComponent.inject(this);

        return mAndroidComponent;
    }

    public AndroidComponent getAndroidComponent() {
        return createComponent();
    }

    public static MyApplication from(Context context) {
        final Context applicationContext = context.getApplicationContext();

        if (applicationContext instanceof MyApplication) {
            return (MyApplication) applicationContext;
        } else {
            throw new IllegalArgumentException("Provided context is not a valid " + MyApplication.class.getCanonicalName() + " instance");
        }
    }

    @VisibleForTesting
    public void putSubcomponentBuilder(Class clazz, SubcomponentBuilder subcomponent) {
        mSubcomponentBuilders = new HashMap<>(mSubcomponentBuilders);
        mSubcomponentBuilders.put(clazz, subcomponent);
    }

    @SuppressWarnings("unchecked")
    public <I, S extends Subcomponent<I>> SubcomponentBuilder<I, S> getSubcomponentBuilder(@NonNull I instance) {
        return (SubcomponentBuilder<I, S>) mSubcomponentBuilders.get(instance.getClass());
    }

}
