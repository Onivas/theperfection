package com.savinoordine.theperfection.networking;

import com.savinoordine.theperfection.model.Stargazer;
import com.savinoordine.theperfection.networking.service.StargazerService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class NetworkManager implements ApiManager {

    private StargazerService mStargazerService;

    @Inject
    public NetworkManager(StargazerService stargazerService) {
        mStargazerService = stargazerService;
    }

    @Override
    public Single<List<Stargazer>> getStargazers(String owner, String repo) {
        return mStargazerService.getStargazers(owner, repo);
    }
}
