package com.savinoordine.theperfection.networking.service;

import com.savinoordine.theperfection.model.Stargazer;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface StargazerService {
    @GET("repos/{owner}/{repo}/stargazers")
    Single<List<Stargazer>> getStargazers(@Path("owner") String owner, @Path("repo") String repo);
}
