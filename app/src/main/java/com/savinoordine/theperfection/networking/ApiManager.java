package com.savinoordine.theperfection.networking;


import com.savinoordine.theperfection.model.Stargazer;

import java.util.List;

import io.reactivex.Single;

public interface ApiManager {

    Single<List<Stargazer>> getStargazers(String owner, String repo);

}
